<?php
namespace Velkashopping\Magento2\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;
use Magento\Framework\Stdlib\CookieManagerInterface;

class OrderObserver implements ObserverInterface
{
    const VID_COOKIE = "vid";
    /**
     * @var \Magento\Framework\Stdlib\CookieManagerInterface
     */
    protected $_cookieManager;

    protected $_orderRepository;

    protected $orderFactory;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    protected $_cookieMetadataFactory;

    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $_sessionManager;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    protected $_remoteAddressInstance;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
     * @param \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory
     */
    public function __construct(
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Sales\Model\Order $orderRepository,
        \Magento\Sales\Api\Data\OrderInterfaceFactory $orderFactory,
        CookieMetadataFactory $cookieMetadataFactory,
        SessionManagerInterface $sessionManager,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->_cookieManager = $cookieManager;
        $this->_orderRepository = $orderRepository;
        $this->orderFactory = $orderFactory;
        $this->_cookieMetadataFactory = $cookieMetadataFactory;
        $this->_sessionManager = $sessionManager;
        $this->_objectManager = $objectManager;
        $this->_remoteAddressInstance = $this->_objectManager->get(
            "Magento\Framework\HTTP\PhpEnvironment\RemoteAddress"
        );
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $orderId = $observer
            ->getEvent()
            ->getOrder()
            ->getIncrementId();

        $vid = $this->_cookieManager->getCookie(self::VID_COOKIE);

        $orderFactory = $this->orderFactory
            ->create()
            ->loadByIncrementId($orderId);

        $_order = $this->_orderRepository->load($orderFactory->getId());
        $_order->setvid($vid);
        $_order->save();
    }
}
