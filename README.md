
# VelkaShopping - Magento 2.4+ tracking

This extension adds the following functionality to Magento:

-   Track visitors from Velkashopping via cookie (Lifetime: 30 days)
-   Send asynchronous order information to Velkashoppings server
-   Expose a Velkashopping uptime monitoring ping url (/velkashopping/v1/ping)

The plugin is tested and confirmed to be working on


-   Magento 2.4

## Usage

### Installation via Composer (Recommended)

 - Install the module composer by running `composer require velkashopping/magento2`
 - Enable the module by running `php bin/magento module:enable Velkashopping_Magento2`
 - Apply database updates by running `php bin/magento setup:upgrade`
 - Compile Magento code base by running `php bin/magento setup:di:compile`
 - Deploy static content by running `php bin/magento setup:static-content:deploy`
 - Clean the cache by running `php bin/magento cache:clean`
 - You are ready to go